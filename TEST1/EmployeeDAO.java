package TEST1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class EmployeeDAO {
    public List<Employeebean> readData(String path) throws  IOException {
      List<Employeebean> list=new ArrayList<Employeebean>();
        BufferedReader bufferedReader= null;
        bufferedReader = new BufferedReader(new FileReader(path));
        String line=null;
        int i=0;
        while((line=bufferedReader.readLine())!=null)
        {
            if(i!=0)
            {
                String[] employeeDetails=line.split(",");
                 Employeebean newEmployee=new Employeebean((Integer.valueOf(employeeDetails[0])), Float.parseFloat(employeeDetails[2]), employeeDetails[1]);
                 list.add(newEmployee);
            }
            i++;
        }
   return list;
    }
    public float getTotSal(List<Employeebean> employeeList)
    {
        Iterator employeeIterator=employeeList.iterator();
        float totalSalary=0;
        while(employeeIterator.hasNext())
        {
            Employeebean employee=(Employeebean) employeeIterator.next();
            totalSalary+=employee.getSalary();
        }
       return totalSalary;
    }
    public int getCount(List<Employeebean> employeeList,float salary)
    {
        /*int totalEmployeeWithGivenSalary=0;
        Iterator employeeIterator=employeeList.iterator();
        while(employeeIterator.hasNext())
        {
            Employeebean employee=(Employeebean) employeeIterator.next();
            if(employee.getSalary()==salary)
            totalEmployeeWithGivenSalary+=1;
        }*/

       int count= (int) employeeList.stream().filter(employeebean -> employeebean.getSalary()==salary).count();
        return count;
    }
    public Employeebean getEmployee(int id,List<Employeebean> employeebeanList) throws UserException
    {
        Employeebean employeebean=null;
        if(employeebeanList.get(id)==null)
        {
            throw  new UserException("Id not found");
        }
        else
        {
            employeebean=employeebeanList.get(id);
        }
      return employeebean;
    }
}
