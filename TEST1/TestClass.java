package TEST1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestClass {

    @Test
    public void testReadData() throws IOException {
        EmployeeDAO employee = new EmployeeDAO();
        List<Employeebean> received = employee.readData("C:\\Users\\prichhab\\Documents\\Sapient\\test\\test1\\TEST1\\bk.csv");
        List<Employeebean> actual = new ArrayList<Employeebean>();
        actual.add(new Employeebean(1, 2,"V"));
        actual.add(new Employeebean(2, 3,"j"));
        actual.add(new Employeebean(3,  9,"S"));
        actual.add(new Employeebean(4 , 2,"p"));
        actual.add(new Employeebean(5, 5,"d"));
        actual.add(new Employeebean(6, 6,"g"));
        actual.add(new Employeebean(7, 2,"f"));
        actual.add(new Employeebean(8, 1 ,"g"));
        actual.add(new Employeebean(9, 3 , "V"));
        actual.add(new Employeebean(10,  6,"e"));
        actual.add(new Employeebean(11, 1 , "df"));
        actual.add(new Employeebean(12, 2 , "d"));
        boolean flag = true;
        for(int i = 0 ; i < received.size() ; i++)
            if(!received.get(i).equals(actual.get(i)))
            {
                flag = false;
                break;
            }
      Assertions.assertEquals(false,flag);
    }
    @Test
    public void totalSalary()
    {
        EmployeeDAO employeeDAO=new EmployeeDAO();
        float totalSalary=0;
        List<Employeebean> list= null;
        try {
            list = employeeDAO.readData("C:\\Users\\prichhab\\Documents\\Sapient\\test\\test1\\TEST1\\bk.csv");
             totalSalary=employeeDAO.getTotSal(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
       Assertions.assertEquals(42.0,totalSalary);
    }

    @Test
    public void countWithSalaryToAmount() {
        EmployeeDAO employeeDAO=new EmployeeDAO();
        int total=0;
        List<Employeebean> list= null;
        try {
            list = employeeDAO.readData("C:\\Users\\prichhab\\Documents\\Sapient\\test\\test1\\TEST1\\bk.csv");
            total=employeeDAO.getCount(list,2);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assertions.assertEquals(4,total);
    }

    @Test
    public  void  employeeFound()
    {

        EmployeeDAO employeeDAO=new EmployeeDAO();
        List<Employeebean> list= null;
        try {
            boolean result=false;
            list = employeeDAO.readData("C:\\Users\\prichhab\\Documents\\Sapient\\test\\test1\\TEST1\\bk.csv");
              if(employeeDAO.getEmployee(10,list)!=null) result=true;
            Assertions.assertEquals(true,result);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UserException e) {
            e.printStackTrace();
        }
    }
}
