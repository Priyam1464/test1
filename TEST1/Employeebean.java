package TEST1;

public class Employeebean {
    private int id;
    private float salary;
    private String name;

    @Override
    public String toString() {
        String employee="Employee id "+id+" has name"+name+" and salary "+salary;
        return employee;
    }

    public Employeebean(int id, float salary, String name) {
        this.id = id;
        this.salary = salary;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public float getSalary() {
        return salary;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }
}
